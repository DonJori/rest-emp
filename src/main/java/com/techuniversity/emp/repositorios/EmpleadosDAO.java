package com.techuniversity.emp.repositorios;

import com.techuniversity.emp.model.Empleado;
import com.techuniversity.emp.model.Empleados;
import com.techuniversity.emp.model.Formacion;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class EmpleadosDAO {

    private static Empleados lista = new Empleados();

    static {
        Formacion form1 = new Formacion("1/22/2019", "Curso Back");
        Formacion form2 = new Formacion("1/22/2020", "Curso Front");
        Formacion form3 = new Formacion("1/22/2021", "Curso SQL");

        ArrayList<Formacion> formaciones1 = new ArrayList<>();
        formaciones1.add(form1);

        ArrayList<Formacion> formaciones2 = new ArrayList<>();
        formaciones2.add(form1);
        formaciones2.add(form2);

        ArrayList<Formacion> formaciones3 = new ArrayList<>();
        formaciones3.add(form3);
        formaciones3.addAll(formaciones2);

        lista.getListaEmpleados().add(new Empleado(1, "Pepe", "Lopez", "pepe@mail.com", formaciones1));
        lista.getListaEmpleados().add(new Empleado(2, "Paco", "Perez", "paco@mail.com", formaciones2));
        lista.getListaEmpleados().add(new Empleado(3, "Antonio", "Martinez", "toni@mail.com", formaciones3));
    }

    public Empleados getAllEmpleados() {
        return lista;
    }

    public Empleado getEmpleado(int id) {
        for (Empleado emp : lista.getListaEmpleados()) {
            if (emp.getId() == id) {
                return emp;
            }
        }

        return null;
    }

    public void addEmpleado(Empleado emp) {
        lista.getListaEmpleados().add(emp);
    }

    public Empleado updateEmpleado(Empleado emp) {
        Empleado currentEmp = getEmpleado(emp.getId());

        if (currentEmp != null) {
            currentEmp.setNombre(emp.getNombre());
            currentEmp.setApellidos(emp.getApellidos());
            currentEmp.setEmail(emp.getEmail());
        }

        return currentEmp;
    }

    public Empleado updateEmpleado(int id, Empleado emp) {
        Empleado currentEmp = getEmpleado(id);

        if (currentEmp != null) {
            currentEmp.setNombre(emp.getNombre());
            currentEmp.setApellidos(emp.getApellidos());
            currentEmp.setEmail(emp.getEmail());
        }

        return currentEmp;
    }

    public void deleteEmpleado(int id) {
        Empleado deleteEmpleado = getEmpleado(id);

        if (deleteEmpleado != null) {
            lista.getListaEmpleados().remove(deleteEmpleado);
        }
    }

    public Empleado patchEmpleado(int id, Map<String, Object> patch) {
        Empleado currentEmpleado = getEmpleado(id);

        if (currentEmpleado != null) {
            for (Map.Entry<String, Object> entrada : patch.entrySet()) {
                modificarValores(currentEmpleado, entrada.getKey(), entrada.getValue());
            }
        }

        return currentEmpleado;
    }

    private static void modificarValores(Empleado currentEmpleado, String key, Object value) {

        switch (key) {
            case "nombre":
                currentEmpleado.setNombre((String) value);
                break;
            case "apellidos":
                currentEmpleado.setApellidos((String) value);
                break;
            case "email":
                currentEmpleado.setEmail((String) value);
                break;
            default:
                break;
        }
    }

    public List<Formacion> getFormacionesEmpleado(int id) {
        Empleado currentEmpleado = getEmpleado(id);

        if (currentEmpleado != null) {
            return currentEmpleado.getFormaciones();
        }

        return null;
    }

    public Empleado addFormacion(int id, Formacion formacion) {
        Empleado currentEmpleado = getEmpleado(id);

        if (currentEmpleado != null) {
            currentEmpleado.getFormaciones().add(formacion);
        }

        return currentEmpleado;
    }
}
