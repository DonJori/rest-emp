package com.techuniversity.emp.utils;

public class Utilidades {

    public static String getCadena(String texto, String separador) throws BadSeparator {
        if (texto.length() < 1 || separador.equals(" ")) throw new BadSeparator();
        String resultado = "";
        char[] letras = texto.trim().toUpperCase().toCharArray();

        for (char letra : letras) {
            resultado += letra;
            if (letra != ' ') resultado += separador;
        }

        return resultado;
    }

    public static boolean esImpar(int number) {
        return (number % 2 != 0);
    }

    public static boolean estaBlanco(String texto) {
        return (texto == null || texto.trim().isEmpty());
    }

    public static boolean valorarEstadoPedido(EstadosPedido estadosPedido) {
        int valor = estadosPedido.ordinal();
        return (valor >= 0 && valor <= 4);
    }

}
