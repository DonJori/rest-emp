package com.techuniversity.emp.controllers;

import com.techuniversity.emp.model.Empleado;
import com.techuniversity.emp.model.Empleados;
import com.techuniversity.emp.model.Formacion;
import com.techuniversity.emp.repositorios.EmpleadosDAO;
import com.techuniversity.emp.utils.Configuracion;
import com.techuniversity.emp.utils.Utilidades;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/empleados")
public class EmpleadosController {

    @Autowired
    private EmpleadosDAO empleadosDAO;

    @GetMapping(path = "/", produces = "application/json")
    public Empleados getEmpleados() {
        return empleadosDAO.getAllEmpleados();
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<Empleado> getEmpleado(@PathVariable int id) {
        Empleado emp = empleadosDAO.getEmpleado(id);

        if (emp != null) {
            return new ResponseEntity<Empleado>(emp, HttpStatus.OK){};
        }

        return ResponseEntity.notFound().build();
    }

    @PostMapping(path = "/", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> addEmpleado(@RequestBody Empleado emp) {
        Integer id = empleadosDAO.getAllEmpleados().getListaEmpleados().size() + 1;
        emp.setId(id);

        empleadosDAO.addEmpleado(emp);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(emp.getId()).toUri();

        return ResponseEntity.created(location).build();
    }

    @PutMapping(path = "/", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> updateEmpleado(@RequestBody Empleado emp) {
        Empleado modEmpleado = empleadosDAO.updateEmpleado(emp);

        if (modEmpleado == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().build();
    }

    @PutMapping(path = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> updateEmpleado(@PathVariable int id, @RequestBody Empleado emp) {
        Empleado modEmpleado = empleadosDAO.updateEmpleado(id, emp);

        if (modEmpleado == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().build();
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Object> deleteEmpleado(@PathVariable int id) {
        empleadosDAO.deleteEmpleado(id);
        return ResponseEntity.ok().build();
    }

    @PatchMapping(path= "/{id}", consumes= "application/json", produces = "application/json")
    public ResponseEntity<Object> patchEmpleado(@PathVariable int id, @RequestBody Map<String, Object> patchVariables) {
        Empleado currentEmpleado = empleadosDAO.patchEmpleado(id, patchVariables);

        if (currentEmpleado == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().build();
    }

    @GetMapping(path = "/{id}/formaciones", produces = "application/json")
    public ResponseEntity<List<Formacion>> getFormaciones(@PathVariable int id) {
        List<Formacion> currentFormaciones = empleadosDAO.getFormacionesEmpleado(id);

        if (currentFormaciones != null) {
            return ResponseEntity.ok().body(currentFormaciones);
        }

        return ResponseEntity.notFound().build();
    }

    @PostMapping(path = "/{id}/formaciones", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> addFormacion(@PathVariable int id, @RequestBody Formacion formacion) {
        Empleado currentEmpleado = empleadosDAO.addFormacion(id, formacion);

        if (currentEmpleado != null) {
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.notFound().build();
    }

    @Value("${app.titulo}") private String titulo;
    @GetMapping(path = "/titulo")
    public String getTitulo() {
        String modo = config.getModo();
        return titulo + " " + modo;
    }

    @Autowired private Environment environment;
    @GetMapping(path = "/prop")
    public String getProperties(@RequestParam("key") String key) {
        String propertyNull = "Sin valor";
        String propertyValor = environment.getProperty(key);

        if (propertyValor != null && !propertyValor.isEmpty()) {
            return propertyValor;
        }

        return propertyNull;
    }

    @Autowired private Configuracion config;
    @GetMapping(path ="/autor")
    public String getAutor() {
        return config.getAutor();
    }

    @GetMapping("/home")
    public String home() {
        return "Don't worry";
    }

    @GetMapping("/cadena")
    public String getCadena(@RequestParam String texto, @RequestParam String separador) {
        try {
            return Utilidades.getCadena(texto, separador);
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }
}
