package com.techuniversity.emp;

import com.techuniversity.emp.controllers.EmpleadosController;
import com.techuniversity.emp.utils.BadSeparator;
import com.techuniversity.emp.utils.Utilidades;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
// @ContextConfiguration(classes = RestEmpApplicationTests.class, loader = AnnotationConfigContextLoader.class)
class RestEmpApplicationTests {

	@Test
	void contextLoads() {
		//assertNotNull(this.getClass());
	}

	@Autowired
	EmpleadosController empleadosController;

	@Test
	public void testHome() {
		String result = empleadosController.home();
		assertEquals("Don't worry", result);
	}

	@Test
	public void testUtilidades() {
		String inicial = "Pepe Botella";
		String result = empleadosController.getCadena(inicial, ".");

		assertEquals("P.E.P.E. B.O.T.E.L.L.A.", result);
	}

	@Test
	public void testBadSeparator() {
		try {
			Utilidades.getCadena("Enric Bala", " ");
			fail("Se esperaba Bad Separator");
		} catch (BadSeparator ex) { }
	}

	@ParameterizedTest
	@ValueSource(ints = {1, 3, 5, -3, 15, Integer.MAX_VALUE})
	public void testEsImpar(int numero) {
		assertTrue(Utilidades.esImpar(numero));
	}
}
